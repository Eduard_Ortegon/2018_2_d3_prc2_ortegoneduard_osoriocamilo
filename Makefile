all: compile run

compile: Sobel.cpp Image.cpp image.h
	g++ -o Sobel Sobel.cpp Image.cpp
run:
	./Sobel

.PHONY: clean
clean:	
	clean rm -rf Sobel


